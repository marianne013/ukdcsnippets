#!/usr/bin/env python
"""
script to configure and submit a BACCARAT job
based on Jordan's writeup
requires a DIRAC UI to be set up (source bashrc)
and a valid proxy: dirac-proxy-init -g lz_user -M
"""

# make stuff look nice, optional
import pprint

# setup DIRAC Scripts
from DIRAC.Core.Base import Script
Script.initialize()
# end of DIRAC setup

from DIRAC.Interfaces.API.Job import Job
from DIRAC.Interfaces.API.Dirac import Dirac

N_OF_JOBS_TO_SUBMIT = 3

def configure_job():
  """
  configures job (one input parameter)
  note that the logfile is copied to the same area as the output
  """
  job = Job()
  job.setInputSandbox(['BACC.sh', 'Pb214.mac'])
  # note that a logfile specified here will be automatically placed
  # in the output sandbox
  job.setExecutable('BACC.sh', logFile='job.log')
  # in case you don't want to hardcode the macro and instead
  # change BACC.sh to take an argument
  # job.setExecutable('BACC.sh', arguments='Pb214.mac')
  job.setName('Pb214_BACC')
  job.setDestination('LCG.UKI-LT2-IC-HEP.uk')
  # Alternatively, let DIRAC pick the site your jobs runs at.
  # You can remove any site you *don't* want your job to go to:
  # job.setBannedSites(['LCG.UKI-SOUTHGRID-BRIS-HEP.uk'])
  job.setPlatform('EL7')
  job.setOutputData(['*.bin'], outputSE='UKI-LT2-IC-HEP-disk')
  # if you have more than one type of output files
  # job.setOutputData(['*.bin', '*.root'], outputSE='UKI-LT2-IC-HEP-disk')
  return job

def submit_and_log(dirac, joblog, job):
  """submits job and stores jobid in a log file for further processing"""
  result = dirac.submitJob(job)
  # atrocious bit of error handling, better than nothing
  jobid = -1
  if result['OK']:
    jobid = result['JobID']
    joblog.write(str(jobid)+'\n')

  return jobid

def check_all_jobs(dirac, logfile_name):
  """Checks status of all jobs submitted so far and prints it to the screen"""
  joblog = open(str(logfile_name), 'r')
  all_jobids = [jobid.strip() for jobid in joblog.readlines()]
  print('\nThe current status of all jobs is:')
  all_status = dirac.getJobStatus(all_jobids)
  pprint.pprint(all_status)
  joblog.close()

def main():
  """does the actual submission"""
  dirac = Dirac()
  logfile_name = "bacc_jobid.log"
  logfile = open(logfile_name, "a")

  job = configure_job()
  for jobcounter in range(N_OF_JOBS_TO_SUBMIT):
    jobid = submit_and_log(dirac, logfile, job)
    if jobid == -1:
      print("Could not submit job.")
  logfile.close()

  check_all_jobs(dirac, logfile_name)

if __name__ == "__main__":
  main()
